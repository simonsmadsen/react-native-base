import React from 'react'
import { AppRegistry } from 'react-native'
import AppContainer from './src'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, combineReducers, compose} from 'redux'
import thunkMiddleware from 'redux-thunk'
import reducer from './src/reducers'
import createLogger from 'redux-logger'
import codePush from 'react-native-code-push'

const loggerMiddleware = createLogger

function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    ),
  )
  return createStore(reducer, initialState, enhancer)
}

const store = configureStore()

const App = () => (
  <Provider store={store}>
    <AppContainer />
  </Provider>
)

AppRegistry.registerComponent('app', () => codePush({
  checkFrequency: codePush.CheckFrequency.ON_APP_START
})(App));
