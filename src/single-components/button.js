import React, { Component } from 'react'
import StyleSheet from 'react-native'
import * as colors from './../style'
import {
  NButton
} from 'react-native-elements'

class Button extends Component {

  constructor(props) {
    super(props)
  }

  render() {
    return (
      <NButton
        raised
        icon={{name: 'cached'}}
        title='RAISED WITH ICON' />
    )
  }
}

export default Button;
