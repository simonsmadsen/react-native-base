import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

class MySpinner extends React.Component {

  constructor(props) {
    super();
    this.state = {
      visible: false
    };
  }

  /* eslint react/no-did-mount-set-state: 0 */
  componentDidMount() {

  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Spinner visible={this.props.visible} textContent={"Updating..."} textStyle={{color: '#FFF'}} />
      </View>
    );
  }
}
export default MySpinner;
