import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { ActionCreators } from './actions'
import { Navigator } from 'react-native'

import Home from './pages/home'
import Page2 from './pages/page2'

class AppContainer extends Component {
  constructor(){
    super()
    this.renderScene = this.renderScene.bind(this)
  }
  renderScene(route,navigator){
    if(route.name == 'home'){
      return <Home {...this.props} navigator={navigator} />
    }else{
      return <Page2 {...this.props} navigator={navigator} />
    }
  }
  render() {
    return <Navigator
      initialRoute={{name: 'home'}}
      renderScene={this.renderScene}
      configureScene={
        (route, routeStack) => Navigator.SceneConfigs.FadeAndroid
      }
    />
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}
export default connect(() => { return {} }, mapDispatchToProps)(AppContainer);
