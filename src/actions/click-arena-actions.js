import Api from '../lib/api'
import fetch from  'fetch';
import * as types from './types'

export function getArenaDataAction() {
  return (dispatch, getState) => {
    dispatch({type: types.DATA_DOWNLOADING})
    fetch.fetch("http://abc.appinto.eu:9999/shop-stats")
    .then( r => r.json())
    .then(r => {
      dispatch(dataReadyAction(r))
    })
  }
}


export function dataReadyAction(data) {
  return {
    type: types.DATA_READY,
    shopData: data
  }
}
