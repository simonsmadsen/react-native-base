import * as DefaultActions from './default-actions'
import * as ClicArenaActions from './click-arena-actions'

export const ActionCreators = Object.assign({},
  DefaultActions,
  ClicArenaActions
)
