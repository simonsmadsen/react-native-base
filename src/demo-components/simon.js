import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class Simon extends Component{

  constructor(props) {
   super(props);
   this.state = {
     name: "Simon",
     is: this.props.is
   }
  }

  render() {
    return (
      <View style={styles.top}>
       <Text>{this.state.name} is {this.state.is}</Text>
     </View>
    );
  }
}

const styles = StyleSheet.create({
  top: {
    width: 50,
    height: 50,
    backgroundColor: 'powderblue'
  },
});
