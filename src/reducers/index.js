import { combineReducers } from 'redux'
import * as defaultReducer from './default'
import * as testReducer from './test'
import * as shopDataReducer from './shop-data'

export default combineReducers(Object.assign(
  defaultReducer,
  testReducer,
  shopDataReducer
))
