import createReducer from '../lib/create-reducer'
import * as types from '../actions/types'
export const _default = createReducer({name:"no name",prossesing: false}, {
  [types.SET_NAME](state, action) {
    return Object.assign({},state,{name:action.name})
  },
  [types.NAME_PROSSESING](state, action) {
    return Object.assign({},state,{prossesing: true})
  },
  [types.NAME_DONE](state, action) {
    return Object.assign({},state,{prossesing: false})
  },
});
