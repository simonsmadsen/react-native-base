import createReducer from '../lib/create-reducer'
import * as types from '../actions/types'

export const test = createReducer({nameSet: false}, {
  [types.SET_NAME](state, action) {
    return Object.assign({},state,{nameSet: true})
  },
})
