import createReducer from '../lib/create-reducer'
import * as types from '../actions/types'

export const shopData = createReducer({cards: [],downloading: false}, {
  [types.DATA_READY](state, action) {
    var cards = action.shopData.map(entry => entry[0])
    return Object.assign({},state,{cards: cards, downloading: false})
  },
  [types.DATA_DOWNLOADING](state,action) {
    return Object.assign({},state,{downloading: true})
  },
})
