import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactNative from 'react-native';
const { View, Text, StyleSheet, TouchableHighlight } = ReactNative;

class Page2 extends Component {
  constructor(props) {
    super(props)
    this.state = {  }
  }


  render() {
    return (
      <View style={styles.scene}>
      <TouchableHighlight onPress={ () => this.props.navigator.pop() }>
         <Text>This is fine</Text>
       </TouchableHighlight>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
    marginTop: 20
  }
});

function mapStateToProps(state) {
  return {

  };
}

export default connect(mapStateToProps)(Page2);
