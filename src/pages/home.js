import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactNative from 'react-native';
const { View, Text, StyleSheet,ScrollView } = ReactNative;
import * as colors from './../style'
import { Card, ListItem,List, Button, SocialIcon } from 'react-native-elements'
import MySpinner from './../single-components/spinner'

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {  }
    this.navigate = this.navigate.bind(this)
  }

  navigate(route){
    this.props.navigator.push('page2')
  }
  componentDidMount(){
    this.props.getArenaDataAction()
    setInterval(function(){
      this.props.getArenaDataAction()
    }.bind(this), 30000);
  }
  cardName(card){
    if(card.buyed == -1){
      return card.name + ' = ' + card.amount
    }else{
        return card.name + ': ' + card.amount + ' = '+card.buyed
    }

  }

  render() {
    return (
      <View style={{paddingTop:20}}>
      <Text style={styles.title}>
        Stats!
      </Text>
      <ScrollView>
      <List containerStyle={{marginBottom: 0}}>
        {
          this.props.cards.map((card, i) => (
              <ListItem
                key={i}
                title={this.cardName(card)}
              />
          ))
        }
      </List>
      <MySpinner visible={this.props.downloading}>
          <Text>This is my custom spinner</Text>
      </MySpinner>
      </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
    flexDirection:'column',
    marginTop: 20,
    backgroundColor:colors.active
  },
  title: {
    letterSpacing:15,
    lineHeight:25,
    height:25,
    color:"#000",
    fontSize:25,
    paddingLeft:10,
    marginTop:10,
    fontWeight:'800'
  }
});

function mapStateToProps(state) {
  return {
    cards: state.shopData.cards,
    downloading: state.shopData.downloading
  };
}

export default connect(mapStateToProps)(Home);
